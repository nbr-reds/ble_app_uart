# ble_app_uart

Firmware for the [nRF52840-DK](https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dk) that make a bridge between the UART and a BLE peripheral using the Nordic UART Service (NUS) server.
It's based on the ble_app_uart example from Nordics and try to have a better modularity by putting the BLE and UART parts on separated files.
For the other side of the communication, the project [usb_nus_c](https://gitlab.com/nbr-reds/usb_nus_c) can be used.

## IDE installation

Follow the instructions found in the [REDS blog](https://blog.reds.ch/?p=1548).

## Usage

Open a terminal with 115200 8N1. Send text ended by CR or/and LF.
