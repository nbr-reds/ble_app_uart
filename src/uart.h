/**
 ******************************************************************************
 * @file    uart.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-December-2022
 * @brief   UART
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef UART_H
#define UART_H

/* Includes ------------------------------------------------------------------*/

#include <stddef.h>
#include <stdint.h>

/* Exported types ------------------------------------------------------------*/

typedef void (*uart_rx_function_t)(uint8_t* data, size_t length);

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the UART
 * @param uart_rx_function the function to call when data are received
 */
void uart_init(uart_rx_function_t uart_rx_function);

/**
 * Transmit data
 * @param data the data to sent
 * @param length the length of the data
 */
void uart_tx(const uint8_t* data, size_t length);

#endif
