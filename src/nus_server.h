/**
 ******************************************************************************
 * @file    nus_server.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    22-June-2020
 * @brief   Nordic UART Service server (peripheral)
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef NUS_SERVER_H
#define NUS_SERVER_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Exported types ------------------------------------------------------------*/

typedef enum {
    NUS_SERVER_EVENT_CONNECTED,
    NUS_SERVER_EVENT_DISCONNECTED,
    NUS_SERVER_EVENT_RX_DATA,
} nus_server_event_t;

typedef void (*nus_server_event_function_t) (nus_server_event_t event, const uint8_t* data, uint16_t length);

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the NUS server
 * @param nus_server_event_function the function to call on event
 */
void nus_server_init(nus_server_event_function_t nus_server_event_function);

/**
 * Transmit data
 * @param data the data to sent
 * @param length the length of the data
 */
void nus_server_tx(const uint8_t* data, size_t length);

/**
 * Enable or disable the BLE
 * @param enable true to enable the BLE
 */
void nus_server_enable(bool enable);

#endif
